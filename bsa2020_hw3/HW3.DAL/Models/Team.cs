﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HW3.DAL.Models
{
    public class Team:TEntity
    {
        public override int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public List<User> UserList { get; set; }
    }
}

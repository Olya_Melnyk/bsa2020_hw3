﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HW3.DAL.Models
{
    public class TaskModel:TEntity
    {
        public override int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskStateModel State { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        public int? PerformerId { get; set; }
        public User Performer { get; set; }
    }
}

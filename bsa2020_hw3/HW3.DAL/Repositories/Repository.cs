﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Newtonsoft.Json;
using HW3.DAL.Models;
using System.Reflection.Metadata.Ecma335;

namespace HW3.DAL.Repositories
{
    public class Repository<T> : IRepository<T> where T:TEntity
    {
        private readonly List<T> _listEntity;
        public Repository(List<T> list) 
        {
            _listEntity = list;
        }
        public List<T> Get(Func<T,bool>filter=null)
        {
            //var list = _listEntity.Where(filter);
            if (filter == null) 
                return _listEntity;
               
            else return (List<T>)_listEntity.Where(filter);
        }
        public void Delete(T entity)
        {
            var item = _listEntity.Contains(entity);
            if (entity == null) throw new InvalidOperationException("Entity was null");
            else if (!item) throw new Exception("In list not found this entity");
            else _listEntity.Remove(entity);
        }
        public void Create(T entity, string createBody=null)
        {
            int index = _listEntity.Count;
            entity.Id = index + 1;
            _listEntity.Add(entity);
        }

        public T Get(int id)
        {
            return _listEntity.FirstOrDefault(e=>e.Id==id);
        }

        public  void Update(T entity, string updateBody=null)
        {
            var updated = _listEntity.FirstOrDefault(e => e.Id == entity.Id);
            if (entity == null) throw new InvalidOperationException("Entity was null");
            else if (updated == null) throw new Exception("In list not found this entity");
            else
            {
                _listEntity.Remove(updated);
                _listEntity.Insert(entity.Id - 1, entity);
            }
        }      

        public void Delete(int id)
        {
            var item = _listEntity.FirstOrDefault(x=>x.Id==id);
            if (item == null) throw new InvalidOperationException("Entity not found");
            else _listEntity.RemoveAt(id-1);
        }

        
    }

}

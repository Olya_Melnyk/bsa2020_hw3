﻿using System;
using System.Collections.Generic;
using System.Text;
using HW3.DAL.Models;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;
using System.IO;

namespace HW3.DAL.Repositories
{
    public class Context
    {
        public List<Project> ProjectList { get;  }
        = JsonConvert.DeserializeObject<List<Project>>(File.ReadAllText("data/projects.json"));
        public List<TaskModel> TaskList { get;  }
        = JsonConvert.DeserializeObject<List<TaskModel>>(File.ReadAllText("data/tasks.json"));
        public List<User> UserList { get; set; }
        = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText("data/users.json"));
        public List<Team> TeamList { get; set; }
        = JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText("data/teams.json"));
    }
}

﻿using HW3.DAL.Abstracts;
using HW3.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace HW3.DAL.Repositories
{
    public class UnitOfWork:IUnitOfWork
    {
        public Context context;
        public UnitOfWork(Context context)
        {
            this.context = context;
        }
        public IRepository<T> GetRepository<T>() where T:TEntity
        {
            return new Repository<T>(GetList<T>());
        }
        public List<T> GetList<T>()
        {
            var type = typeof(T);
            if (type == typeof(User))
            {
                return context.UserList as List<T>;
            }
            else if (type == typeof(Project))
            {
                return context.ProjectList as List<T>;
            }
            else if (type == typeof(Team))
            {
                return context.TeamList as List<T>;
            }
            else if (type == typeof(TaskModel))
            {
                return context.TaskList as List<T>;
            }
            else throw new Exception($"Haven`t got list of this type {type}");
        }
    }
}

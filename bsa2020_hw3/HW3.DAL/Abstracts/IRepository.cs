﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq.Expressions;
using System.Text;

namespace HW3.DAL.Repositories
{
    public interface IRepository<T> where T:TEntity
    {
        List<T> Get(Func<T,bool> filter=null);
        T Get(int id);
        void Create(T entity, string createBody = null);
        void Update(T entity, string modifieBody = null);
        void Delete(T entity);
        void Delete(int id);
    }
}
